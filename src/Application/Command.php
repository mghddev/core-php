<?php

namespace XCompany\Core\Application;

interface Command
{
    /**
     * Get payload value by key
     *
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    public function all(): array;

    public function commandName(): string;
}
