<?php

namespace XCompany\Core\Application;

interface CommandHandler
{
    public function handle(Command $command);
}
