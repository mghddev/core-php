<?php

namespace XCompany\Core\Application;

interface CommandHandlerLocator
{
    /**
     * @param Command $command
     * @return CommandHandler
     * @throws CommandHandlerNotFoundException
     */
    public function locate(Command $command): CommandHandler;
}
