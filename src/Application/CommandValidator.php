<?php

namespace XCompany\Core\Application;

interface CommandValidator
{
    public function validate(Command $command): CommandValidationResult;
}
