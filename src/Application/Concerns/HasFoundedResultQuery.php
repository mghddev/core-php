<?php

namespace XCompany\Core\Application\Concerns;

trait HasFoundedResultQuery
{
    public function found(): bool
    {
        return true;
    }
}
