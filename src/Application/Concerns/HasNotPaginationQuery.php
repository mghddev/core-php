<?php

namespace XCompany\Core\Application\Concerns;

trait HasNotPaginationQuery
{
    public function skip(): int
    {
        return 0;
    }

    public function take(): int
    {
        return 1;
    }
}
