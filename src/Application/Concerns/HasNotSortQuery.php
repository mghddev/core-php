<?php

namespace XCompany\Core\Application\Concerns;

trait HasNotSortQuery
{
    public function sorts(): array
    {
        return [];
    }
}
