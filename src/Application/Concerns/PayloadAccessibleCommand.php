<?php

namespace XCompany\Core\Application\Concerns;

trait PayloadAccessibleCommand
{
    public function get(string $key)
    {
        $payload = $this->all();

        return $payload[$key] ?? null;
    }

    public function all(): array
    {
        $payload = [];

        $reflect = new \ReflectionObject($this);
        foreach ($reflect->getProperties() as $property) {
            $property->setAccessible(true);
            $payload[$property->getName()] = $property->getValue($this);
        }

        return $payload;
    }
}
