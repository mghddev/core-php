<?php

namespace XCompany\Core\Application;

use Assert\Assertion;

/**
 * Class GenericQuery
 * @package XCompany\Core\Application
 */
final class GenericQuery implements Query
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $term;

    /**
     * @var array
     */
    private $filters;

    /**
     * @var array
     */
    private $sorts;

    /**
     * @var int
     */
    private $take;

    /**
     * @var int
     */
    private $skip;

    /**
     * GenericQuery constructor.
     * @param string $name
     * @param string|null $term
     * @param array $filters
     * @param array $sorts
     * @param int $take
     * @param int $skip
     */
    public function __construct(
        string $name,
        ?string $term = null,
        array $filters = [],
        array $sorts = [],
        int $take = 1,
        int $skip = 0
    ) {
        Assertion::min($take, 1);
        Assertion::min($skip, 0);
        $this->name = $name;
        $this->term = $term;
        $this->filters = array_filter($filters, function ($filter) {
            return $filter instanceof Filter;
        });

        $this->sorts = array_filter($sorts, function ($sort) {
            return $sort instanceof Sort;
        });
        $this->take = $take;
        $this->skip = $skip;
    }

    /**
     * @return array|Filter[]
     */
    public function filters(): array
    {
        return $this->filters;
    }

    /**
     * @return Sort[]
     */
    public function sorts(): array
    {
        return $this->sorts;
    }

    /**
     * @return string
     */
    public function term(): string
    {
        return $this->term;
    }

    /**
     * @return int
     */
    public function skip(): int
    {
        return $this->skip;
    }

    /**
     * @return int
     */
    public function take(): int
    {
        return $this->take;
    }

    /**
     * @return string
     */
    public function queryName(): string
    {
        return $this->name;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        if (array_key_exists($key, $this->all())) {
            return $this->all()[$key];
        }

        throw new \InvalidArgumentException('Invalid key');
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return [
            'filters' => $this->filters(),
            'sorts' => $this->sorts(),
            'take' => $this->take(),
            'skip' => $this->skip(),
            'term' => $this->term(),
        ];
    }
}
