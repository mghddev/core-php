<?php

namespace XCompany\Core\Application;

interface Query
{
    /**
     * @return string
     */
    public function queryName(): string;

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * @return array
     */
    public function all(): array;
}
