<?php

namespace XCompany\Core\Application;

interface QueryBus
{
    /**
     * @param Query $query
     * @return mixed
     */
    public function ask(Query $query);
}
