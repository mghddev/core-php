<?php

namespace XCompany\Core\Application;

interface QueryHandler
{
    /**
     * @param Query|GenericQuery $query
     * @return mixed
     */
    public function handle(Query $query);
}
