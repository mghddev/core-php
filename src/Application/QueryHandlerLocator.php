<?php

namespace XCompany\Core\Application;

interface QueryHandlerLocator
{
    /**
     * @param Query $query
     * @return QueryHandler
     * @throws QueryHandlerNotFoundException
     */
    public function locate(Query $query): QueryHandler;
}
