<?php

namespace XCompany\Core\Application;

use Assert\Assertion;

final class Sort implements \JsonSerializable
{
    private const DESCENDING = 1;
    private const ASCENDING = 2;

    /**
     * @var string
     */
    private $on;

    /**
     * @var int
     */
    private $method;

    public function __construct(string $on, int $method)
    {
        Assertion::inArray($method, [self::DESCENDING, self::ASCENDING]);

        $this->on = $on;
        $this->method = $method;
    }

    public function on(): string
    {
        return $this->on;
    }

    public function isDesc(): bool
    {
        return $this->method === self::DESCENDING;
    }

    public static function desc(string $field): Sort
    {
        return new self($field, self::DESCENDING);
    }

    public static function asc(string $field): Sort
    {
        return new self($field, self::ASCENDING);
    }

    public function equalsTo(Sort $sort): bool
    {
        return $this->on() === $sort->on()
            && $this->isDesc() === $sort->isDesc();
    }

    public function __toString()
    {
        return json_encode([
            'field' => $this->on,
            'is_desc' => $this->isDesc()
        ]);
    }

    public function jsonSerialize()
    {
        return (string)$this;
    }
}
