<?php

namespace XCompany\Core\Application;

interface TransactionManager
{
    public function begin(): void;

    public function rollback(): void;

    public function commit(): void;

    public function isZeroLevel(): bool;
}
