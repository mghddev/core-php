<?php

namespace XCompany\Core\Common;

interface Event
{
    /**
     * Get payload value by key
     *
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    public function all(): array;

    public function eventName(): string;
}
