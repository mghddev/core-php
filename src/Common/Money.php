<?php

namespace XCompany\Core\Common;

use Money\Money as BaseMoney;

final class Money implements \JsonSerializable
{
    /**
     * @var BaseMoney
     */
    private $money;

    public function __construct($amount)
    {
        $this->money = BaseMoney::IRR($amount);
    }

    public function equalsTo(Money $another)
    {
        return $this->getAmount() === $another->getAmount();
    }

    public function getAmount()
    {
        return $this->money->getAmount();
    }

    /**
     * @param Money $another
     * @return Money
     */
    public function add(Money $another)
    {
        $money = BaseMoney::IRR($another->getAmount());
        $newMoney = $this->money->add($money);

        return new static($newMoney->getAmount());
    }

    /**
     * @param Money $another
     * @return Money
     */
    public function subtract(Money $another)
    {
        $money = BaseMoney::IRR($another->getAmount());
        $newMoney = $this->money->subtract($money);

        return new static($newMoney->getAmount());
    }

    /**
     * @param $multiplier
     * @return Money
     */
    public function multiply($multiplier)
    {
        return new static($this->money->multiply($multiplier)->getAmount());
    }

    /**
     * @param $devisor
     * @return Money
     */
    public function devide($devisor)
    {
        return new static($this->money->divide($devisor)->getAmount());
    }

    public function __toString()
    {
        return (string)$this->getAmount();
    }

    public function lessThan(Money $another)
    {
        return $this->money->lessThan(BaseMoney::IRR($another->getAmount()));
    }

    public function lessOrEquals(Money $another)
    {
        return $this->lessThan($another) || $this->equalsTo($another);
    }

    public function moreThan(Money $another)
    {
        return $this->money->greaterThan(BaseMoney::IRR($another->getAmount()));
    }

    public function absolute()
    {
        return new self(abs($this->getAmount()));
    }

    public static function aZero()
    {
        return new self(0);
    }

    /**
     * @return bool
     */
    public function hasNegativeAmount()
    {
        return $this->lessThan(Money::aZero());
    }

    public function toNegative()
    {
        return new Money($this->getAmount() * (-1));
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return (string)$this;
    }
}
