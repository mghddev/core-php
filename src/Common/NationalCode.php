<?php

namespace XCompany\Core\Common;


final class NationalCode
{
    /**
     * @var string
     */
    private $nationalCode;

    /**
     * NationalCode constructor.
     * @param string $nationalCode
     */
    public function __construct(string $nationalCode)
    {
        if (self::validate($nationalCode) === false) {
            throw new \InvalidArgumentException('NationalCode is not valid');
        }

        $this->nationalCode = $nationalCode;
    }

    /**
     * @param string $nationalCode
     * @return bool
     */
    public static function validate(string $nationalCode)
    {
        if (strlen($nationalCode) !== 10) {
            return false;
        }

        if (!is_numeric($nationalCode)) {
            return false;
        }

        $numbers = str_split($nationalCode);
        $sum = 0;
        for ($i = 0; $i < 9; $i++) {
            $sum += (10 - $i) * (int)$numbers[$i];
        }

        $remained = $sum % 11;

        if ($remained < 2) {
            return $remained === (int)$numbers[9];
        }

        return (11 - $remained) === (int)$numbers[9];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nationalCode;
    }

    /**
     * @return string
     */
    public function value()
    {
        return $this->nationalCode;
    }

    /**
     * @param NationalCode $other
     * @return bool
     */
    public function equalsTo(NationalCode $other): bool
    {
        return $this->value() === $other->value();
    }
}
