<?php

namespace XCompany\Core\Common;

final class NationalIdentity
{
    /**
     * @var string
     */
    private $nationalIdentity;

    /**
     * NationalIdentity constructor.
     * @param string $nationalIdentity
     */
    public function __construct(string $nationalIdentity)
    {
        if (self::validate($nationalIdentity) === false) {
            throw new \InvalidArgumentException('Invalid National Identifier');
        }

        $this->nationalIdentity = $nationalIdentity;
    }

    /**
     * Check correctness of given national identifier
     *
     * @link http://www.aliarash.com/article/shenasameli/shenasa_meli.htm
     * @param string $nationalIdentity
     * @return bool
     */
    public static function validate(string $nationalIdentity)
    {
        if (is_numeric($nationalIdentity) === false) {
            return false;
        }

        if (strlen($nationalIdentity) !== 11) {
            return false;
        }

        $controlNumber = (int)substr($nationalIdentity, 10);
        $numbers = str_split(substr($nationalIdentity, 0, 10));
        $adder = (int)substr($nationalIdentity, 9, 1) + 2;
        $multipleMap = [29, 27, 23, 19, 17, 29, 27, 23, 19, 17];

        $sum = 0;
        for ($i = 0; $i < 10; $i++) {
            $sum += ($adder + (int)$numbers[$i]) * $multipleMap[$i];
        }

        $remained = $sum % 11;
        if ($remained === 10) {
            $remained = 0;
        }

        return $remained === $controlNumber;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nationalIdentity;
    }

    /**
     * @return string
     */
    public function value()
    {
        return $this->nationalIdentity;
    }


    /**
     * @param NationalIdentity $other
     * @return bool
     */
    public function equalsTo(NationalIdentity $other): bool
    {
        return $this->value() === $other->value();
    }
}
