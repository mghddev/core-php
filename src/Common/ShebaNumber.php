<?php

namespace XCompany\Core\Common;

final class ShebaNumber
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $fullNumber;

    public function __construct(string $number)
    {
        if (self::validate($number) === false) {
            throw new \InvalidArgumentException('Invalid sheba number');
        }

        if (strlen($number) === 24) {
            $this->number = $number;
            $this->fullNumber = 'IR' . $number;
        } else {
            $this->fullNumber = $number;
            $this->number = substr($this->fullNumber, 2, 26);
        }
    }

    /**
     * @link https://fa.wikipedia.org/wiki/%D8%A7%D9%84%DA%AF%D9%88%D8%B1%DB%8C%D8%AA%D9%85_%DA%A9%D8%AF_%D8%B4%D8%A8%D8%A7
     * @param string $number
     * @return bool
     */
    public static function validate(string $number)
    {
        $length = strlen($number);
        if (false === in_array($length, [26, 24])) {
            return false;
        }

        if ($length === 24) {
            $number = 'IR' . $number;
        }

        if (starts_with($number, 'IR') === false) {
            return false;
        }

        $prefix = str_replace('IR', '1827', substr($number, 0, 4));
        $number = substr($number, 4, strlen($number)) . $prefix;


        if (is_numeric($number) === false) {
            return false;
        }

        return (int)bcmod($number, 97) === 1;
    }

    public function fullNumber()
    {
        return $this->fullNumber;
    }

    public function number()
    {
        return $this->number;
    }

    public function __toString()
    {
        return $this->fullNumber();
    }

    public function equalsTo(ShebaNumber $number)
    {
        return $this->fullNumber() === $number->fullNumber();
    }

    public function countryCode()
    {
        return 'IR';
    }
}
