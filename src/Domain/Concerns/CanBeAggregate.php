<?php

namespace XCompany\Core\Domain\Concerns;

use XCompany\Core\Domain\DomainEvent;

trait CanBeAggregate
{
    protected $recordedEvents = [];

    public function recordThat(DomainEvent $event): void
    {
        $this->recordedEvents[] = $event;
    }

    public function getRecordedEvents(): array
    {
        $events = $this->recordedEvents;
        $this->recordedEvents = [];

        return $events;
    }
}
