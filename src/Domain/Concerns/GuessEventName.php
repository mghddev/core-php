<?php

namespace XCompany\Core\Domain\Concerns;

trait GuessEventName
{
    public function eventName(): string
    {
        $base = explode('\\', get_class($this));

        return end($base);
    }
}
