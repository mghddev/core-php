<?php

namespace XCompany\Core\Domain;

interface Repository
{
    public function find($id): ?AggregateRoot;

    public function save(AggregateRoot $aggregateRoot): void;
}
