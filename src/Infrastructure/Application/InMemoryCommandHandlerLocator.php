<?php

namespace XCompany\Core\Infrastructure\Application;

use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandHandler;
use XCompany\Core\Application\CommandHandlerLocator;
use XCompany\Core\Application\CommandHandlerNotFoundException;

final class InMemoryCommandHandlerLocator implements CommandHandlerLocator
{
    /**
     * @var array
     */
    private $handlers;

    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
    }

    public function locate(Command $command): CommandHandler
    {
        if (array_key_exists($command->commandName(), $this->handlers)) {
            return $this->handlers[$command->commandName()];
        }

        throw new CommandHandlerNotFoundException();
    }

    public function addHandler(string $commandName, CommandHandler $handler)
    {
        $this->handlers[$commandName] = $handler;

        return $this;
    }
}
