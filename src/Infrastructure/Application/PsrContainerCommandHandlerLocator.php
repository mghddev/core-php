<?php

namespace XCompany\Core\Infrastructure\Application;

use Illuminate\Contracts\Container\Container;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandHandler;
use XCompany\Core\Application\CommandHandlerLocator;
use XCompany\Core\Application\CommandHandlerNotFoundException;
use XCompany\Core\Application\GenericCommand;

final class PsrContainerCommandHandlerLocator implements CommandHandlerLocator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function locate(Command $command): CommandHandler
    {
        try {
            $handlerName = $command instanceof GenericCommand
                ? $command->commandName() . 'Handler'
                : get_class($command) . 'Handler';
            return $this->container->make($handlerName);
        } catch (\Exception $e) {
            if ($e instanceof NotFoundExceptionInterface || $e instanceof ContainerExceptionInterface) {
                throw new CommandHandlerNotFoundException();
            }

            throw $e;
        }
    }
}
