<?php

namespace XCompany\Core\Infrastructure\Application;

use XCompany\Core\Application\Query;
use XCompany\Core\Application\QueryBus;
use XCompany\Core\Application\QueryHandlerLocator;

final class SimpleQueryBus implements QueryBus
{
    /**
     * @var QueryHandlerLocator
     */
    private $handlerLocator;

    public function __construct(QueryHandlerLocator $handlerLocator)
    {
        $this->handlerLocator = $handlerLocator;
    }

    public function ask(Query $query)
    {
        $handler = $this->handlerLocator->locate($query);
        
        return $handler->handle($query);
    }
}
