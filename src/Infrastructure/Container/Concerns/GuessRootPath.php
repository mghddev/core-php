<?php

namespace XCompany\Core\Infrastructure\Container\Concerns;

trait GuessRootPath
{
    public function getRootPath(): string
    {
        $ref =  new \ReflectionClass(get_called_class());
        return dirname($ref->getFileName());
    }
}
