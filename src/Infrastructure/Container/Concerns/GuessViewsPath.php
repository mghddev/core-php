<?php

namespace XCompany\Core\Infrastructure\Container\Concerns;

trait GuessViewsPath
{
    public function getViewsPath(): array
    {
        $dirParts = explode('/', $this->getRootPath());

        return [
            $this->getRootPath() . '/Presentation/Web/Views',
            camel_case($dirParts[count($dirParts) - 2]),
        ];
    }
}
