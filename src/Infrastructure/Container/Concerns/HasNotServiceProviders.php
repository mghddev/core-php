<?php

namespace XCompany\Core\Infrastructure\Container\Concerns;

trait HasNotServiceProviders
{
    public function getServiceProviders(): array
    {
        return [];
    }
}
