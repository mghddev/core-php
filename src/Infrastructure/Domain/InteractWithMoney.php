<?php

namespace XCompany\Core\Infrastructure\Domain;

use XCompany\Core\Common\Money;

/**
 * Trait InteractWithMoney
 * @package XCompany\Core\Infrastructure\Domain
 * @property string[] $moneyFields
 */
trait InteractWithMoney
{
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->moneyFields) && $value instanceof Money) {
            return $this->setAttribute($key, $value->getAmount());
        }

        return parent::setAttribute($key, $value);
    }

    public function getMoney(string $key): Money
    {
        $value = $this->getAttribute($key);

        if (in_array($key, $this->moneyFields) && !($value instanceof Money)) {
            if ($value === null) {
                return null;
            }

            return new Money($value);
        }

        return $value;
    }

    public function scopeMoney($query, Money $money, string $key, string $operator = '=', string $conditionType = 'and')
    {
        $sub = function ($sub) use ($money, $key, $operator) {
            return $sub->where($key, $operator, $money->getAmount());
        };

        if ($conditionType === 'or') {
            return $query->orWhere($sub);
        }

        return $query->where($sub);
    }

    public function scopeOrMoney($query, Money $money, string $key, string $operator = '=')
    {
        return $query->money($money, $key, $operator, 'or');
    }

    public function scopeMoneyNot($query, Money $money, string $key, string $conditionType = 'and')
    {
        return $query->money($query, $money, $key, '!=', $conditionType);
    }
}
