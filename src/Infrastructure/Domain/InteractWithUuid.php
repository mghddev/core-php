<?php

namespace XCompany\Core\Infrastructure\Domain;


use XCompany\Core\Common\Uuid;

/**
 * Trait InteractWithUuid
 * @package Jobinja\Business\Ecommerce\Infrastructure\Domain
 * @method uuid(Uuid $id, string $key = null)
 * @method orUuid(Uuid $id, string $key = null)
 */
trait InteractWithUuid
{
    public function getUuid($key)
    {
        $value = $this->getAttribute($key);

        if (in_array($key, $this->uuids) && !($value instanceof Uuid)) {
            if ($value === null) {
                return null;
            }

            return Uuid::fromBytes($value);
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->uuids) && $value instanceof Uuid) {
            return $this->setAttribute($key, $value->toBytes());
        }

        return parent::setAttribute($key, $value);
    }

    public function scopeUuid($query, Uuid $uuid, $key = null, $operator = '=', $conditionType = 'and')
    {
        if ($key === null) {
            $key = $this->getKeyName();
        }

        if ($conditionType === 'or') {
            return $query->orWhere($key, $operator, $uuid->toBytes());
        }

        return $query->where($key, $operator, $uuid->toBytes());
    }

    public function scopeOrUuid($query, Uuid $uuid, $key = null, $operator = '=')
    {
        return $query->uuid($uuid, $key, $operator, 'or');
    }

    public function scopeUuidNot($query, Uuid $uuid, $key = null, $conditionType = 'and')
    {
        return $query->uuid($uuid, $key, '!=', $conditionType);
    }

    public function toArray()
    {
        $array = parent::toArray();
        if (isset($this->uuids) === false || !is_array($this->uuids)) {
            return $array;
        }

        foreach ($this->uuids as $field) {
            if (array_key_exists($field, $array)) {
                $array[$field] = $this->getUuid($field);
            }
        }

        return $array;
    }
}
