<?php

namespace XCompany\Core\Infrastructure;

use XCompany\Core\Common\Concerns\PayloadAccessibleEvent;
use XCompany\Core\Common\Event;
use XCompany\Core\Domain\Concerns\GuessEventName;

final class ExceptionEvent implements Event
{
    use PayloadAccessibleEvent;
    use GuessEventName;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $trace;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int|mixed
     */
    private $code;

    /**
     * @var int
     */
    private $line;

    /**
     * @var string
     */
    private $file;

    /**
     * ExceptionEvent constructor.
     * @param \Throwable $exception
     */
    public function __construct(\Throwable $exception)
    {
        $this->name = get_class($exception);
        $this->trace = $exception->getTraceAsString();
        $this->message = $exception->getMessage();
        $this->code = $exception->getCode();
        $this->line = $exception->getLine();
        $this->file = $exception->getFile();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTrace(): string
    {
        return $this->trace;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int|mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }
}
