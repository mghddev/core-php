<?php

namespace XCompany\Core\Infrastructure;

use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;
use XCompany\Core\Application\CommandBus;
use XCompany\Core\Application\EventDispatcher;
use XCompany\Core\Application\QueryBus;
use XCompany\Core\Application\TransactionManager;
use XCompany\Core\Application\UnitOfWork;
use XCompany\Core\Infrastructure\Application\SimpleCommandBus;
use XCompany\Core\Infrastructure\Application\SimpleQueryBus;
use XCompany\Core\Infrastructure\Application\TransactionableCommandBus;
use XCompany\Core\Infrastructure\Container\ContainerProvider;

final class LaravelServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $containers = $this->getRegisteredContainers();

        while ($container = array_shift($containers)) {
            $this->registerContainerRoutes($container);
            $this->registerContainerViewPaths($container);
            $this->registerContainerMigrations($container);
            $this->registerConsoleCommands($container);
            $this->registerEventsAndListeners($container);
        }
    }

    /**
     * @return ContainerProvider[]
     */
    private function getRegisteredContainers(): array
    {
        return array_map(function ($container) {
            return $this->app->make($container);
        }, $this->app->make('config')->get('containers', []));
    }

    private function registerServiceProviders(ContainerProvider $provider): void
    {
        foreach ($provider->getServiceProviders() as $serviceProvider) {
            $this->app->register($serviceProvider);
        }
    }

    private function registerContainerRoutes(ContainerProvider $provider): void
    {
        foreach ($provider->getRoutesPaths() as $path) {
            if (file_exists($path)) {
                $this->loadRoutesFrom($path);
            }
        }
    }

    private function registerContainerViewPaths(ContainerProvider $provider): void
    {
        $viewPath = $provider->getViewsPath();
        if (file_exists($viewPath[0])) {
            $this->loadViewsFrom($viewPath[0], $viewPath[1]);
        }
    }

    private function registerContainerMigrations(ContainerProvider $provider): void
    {
        if (file_exists($provider->getMigrationsPath())) {
            $this->loadMigrationsFrom($provider->getMigrationsPath());
        }
    }

    private function registerConsoleCommands(ContainerProvider $provider): void
    {
        foreach ($provider->getConsoleCommands() as $command) {
            $this->commands($command);
        }
    }

    public function register(): void
    {
        $this->registerTransactionManager();
        $this->registerEventDispatcher();
        $this->registerUnitOfWork();
        $this->registerCommandBus();
        $this->registerQueryBus();

        $containers = $this->getRegisteredContainers();
        while ($container = array_shift($containers)) {
            $this->registerServiceProviders($container);
            $this->registerBindings($container);
        }
    }

    private function registerTransactionManager(): void
    {
        $this->app->singleton(TransactionManager::class, function (Container $app) {
            return new EloquentTransactionManager($app->make('db.connection'));
        });
    }

    private function registerEventDispatcher(): void
    {
        $this->app->singleton(EventDispatcher::class, function (Container $app) {
            return new LaravelEventDispatcher($app->make('events'));
        });
    }

    private function registerCommandBus(): void
    {
        $this->app->singleton(CommandBus::class, function ($app) {
            return new TransactionableCommandBus(
                $app->make(SimpleCommandBus::class),
                $app->make(UnitOfWork::class)
            );
        });
    }

    private function registerUnitOfWork(): void
    {
        $this->app->singleton(UnitOfWork::class, function ($app) {
            return new UnitOfWork(
                $app->make(TransactionManager::class),
                $app->make(EventDispatcher::class)
            );
        });
    }

    private function registerQueryBus(): void
    {
        $this->app->singleton(QueryBus::class, SimpleQueryBus::class);
    }

    private function registerBindings(ContainerProvider $provider): void
    {
        foreach ($provider->getBindings() as $abstract => $resolver) {
            $this->app->singleton($abstract, $resolver);
        }
    }

    private function registerEventsAndListeners(ContainerProvider $provider): void
    {
        $dispatcher = $this->app->make('events');

        foreach ($provider->getEvents() as $event => $listeners) {
           foreach (array_unique($listeners) as $listener) {
               $dispatcher->listen($event, $listener);
           }
        }
    }
}
