<?php

namespace XCompany\Core\Infrastructure\Migration;

use Illuminate\Database\Schema\Blueprint as BaseBlueprint;

final class Blueprint extends BaseBlueprint
{
    public function uuidAsBinary($column, $length = 16)
    {
        return $this->addColumn('binaryUuid', $column, compact('length'));
    }

    public function uuidAsBinaryPrimary($column, $length = 16)
    {
        return $this->uuidAsBinary($column, $length)->primary();
    }

    public function money($column)
    {
        return $this->decimal($column, 18, 4);
    }

    public function percentage($column)
    {
        return $this->decimal($column, 5, 2);
    }

    public function equity($column)
    {
        return $this->decimal($column, 8, 4);
    }
}
