<?php

namespace XCompany\Core\Infrastructure\Migration;

trait ExtendSchemaBuilder
{
    protected function getExtendedSchemaBuilder()
    {
        $drivers = ['sqlite', 'mysql'];

        if (in_array(\DB::connection()->getName(), $drivers)) {
            switch (\DB::connection()->getName()) {
                case 'sqlite':
                    \DB::connection()->setSchemaGrammar(new SQLiteSchemaGrammar());
                    break;

                case 'mysql':
                    \DB::connection()->setSchemaGrammar(new MysqlSchemaGrammar());
                    break;
            }

            $schema = \DB::connection()->getSchemaBuilder();
            $schema->blueprintResolver(function ($table, $callback) {
                return new Blueprint($table, $callback);
            });

            return $schema;
        }

        return \DB::connection()->getSchemaBuilder();
    }
}
