<?php

namespace XCompany\Core\Infrastructure\Migration;

use Illuminate\Database\Schema\Grammars\SQLiteGrammar;
use Illuminate\Support\Fluent;

final class SQLiteSchemaGrammar extends SQLiteGrammar
{
    public function typeBinaryUuid(Fluent $column)
    {
        return $this->typeBinary($column);
    }
}
