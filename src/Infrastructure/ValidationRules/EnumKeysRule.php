<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

final class EnumKeysRule implements Rule
{
    /**
     * @var string
     */
    private $enumType;

    public function __construct(string $enumType)
    {
        $this->enumType = $enumType;
    }

    public function passes($attribute, $value)
    {
        $keys = call_user_func([$this->enumType, 'keys']);

        if (in_array($value, $keys)) {
            return true;
        }

        $keys = array_map(function ($key) {
            return Str::lower($key);
        }, $keys);

        return in_array($value, $keys);
    }

    public function message()
    {
        return ':attribute is not valid';
    }
}
