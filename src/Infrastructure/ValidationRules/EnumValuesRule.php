<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;

final class EnumValuesRule implements Rule
{

    /**
     * @var string
     */
    private $enumType;

    public function __construct(string $enumType)
    {
        $this->enumType = $enumType;
    }

    public function passes($attribute, $value)
    {
        return in_array($value, call_user_func([$this->enumType, 'values']));
    }

    public function message()
    {
        return ':attribute is not valid';
    }
}
