<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;
use XCompany\Core\Common\NationalIdentity;

final class NationalIdentityRule implements Rule
{
    public function passes($attribute, $value)
    {
        return NationalIdentity::validate($value);
    }

    public function message()
    {
        return ':attribute is not valid national identity';
    }
}
