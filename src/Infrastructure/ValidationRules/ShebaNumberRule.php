<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;
use XCompany\Core\Common\ShebaNumber;

final class ShebaNumberRule implements Rule
{
    public function passes($attribute, $value)
    {
        return ShebaNumber::validate($value);
    }

    public function message()
    {
        return ':attribute is not valid sheba number';
    }
}
