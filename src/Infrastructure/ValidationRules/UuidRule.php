<?php

namespace XCompany\Core\Infrastructure\ValidationRules;

use Illuminate\Contracts\Validation\Rule;
use XCompany\Core\Common\Uuid;

final class UuidRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Uuid::isValid($value);
    }

    public function message()
    {
        return ':attribute is invalid uuid';
    }
}
