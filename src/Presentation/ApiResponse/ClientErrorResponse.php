<?php

namespace XCompany\Core\Presentation\ApiResponse;

use Assert\Assertion;
use Illuminate\Http\JsonResponse;

class ClientErrorResponse implements Response, HasJsonResponse
{
    use InteractWithHeaders;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array
     */
    private $meta;

    /**
     * @var array
     */
    private $headers;

    public function __construct(string $message, int $statusCode, array $data, array $meta, array $headers)
    {
        Assertion::greaterOrEqualThan($statusCode, 400);
        Assertion::lessThan($statusCode, 500);

        $this->message = $message;
        $this->statusCode = $statusCode;
        $this->data = $data;
        $this->meta = $meta;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function toJsonResponse(): JsonResponse
    {
        $data = [
            'message' => $this->message,
        ];

        if ($this->statusCode === 422) {
            $data['errors'] = $this->data;
            $data['meta'] = $this->meta;
        }

        return new JsonResponse(
            $data,
            $this->statusCode,
            $this->headers
        );
    }
}
