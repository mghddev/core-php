<?php

namespace XCompany\Core\Presentation\ApiResponse;

use Illuminate\Http\JsonResponse;

interface HasJsonResponse
{
    public function toJsonResponse(): JsonResponse;
}
