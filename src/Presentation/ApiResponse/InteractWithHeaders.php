<?php

namespace XCompany\Core\Presentation\ApiResponse;

trait InteractWithHeaders
{
    public function withHeader(string $key, $value): Response
    {
        return new static(
            $this->getMessage(),
            $this->getStatusCode(),
            $this->getData(),
            $this->getMeta(),
            array_merge($this->getHeaders(), [
                $key => $value,
            ])
        );
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
