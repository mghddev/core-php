<?php

namespace XCompany\Core\Presentation\ApiResponse;

interface Response
{
    public function getMessage(): string;

    public function getStatusCode(): int;

    public function getData(): array;

    public function getMeta(): array;

    public function getHeaders(): array;

    public function withHeader(string $key, $value): Response;
}
