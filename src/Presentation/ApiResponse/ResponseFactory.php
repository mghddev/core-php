<?php


namespace XCompany\Core\Presentation\ApiResponse;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\Paginator;

final class ResponseFactory
{
    /**
     * @param array $errors
     * @param array $meta
     * @param array $headers
     * @return ClientErrorResponse
     */
    public static function validation(array $errors, array $meta = [], array $headers = []): ClientErrorResponse
    {
        return new ClientErrorResponse('Validation error', 422, $errors, $meta, $headers);
    }

    /**
     * @param array $items
     * @param array $paginationMeta
     * @param array $meta
     * @param array $headers
     * @return OkResponse
     */
    public static function pagination(array $items, array $paginationMeta, array $meta = [], array $headers = []): OkResponse
    {
        return new OkResponse('Found', 200, $items, array_merge($meta, [
            'pagination' => $paginationMeta,
        ]), $headers);
    }

    public static function fromLaravelPaginator(Paginator $paginator, array $meta = []): OkResponse
    {
        $paginationMeta = [
            'current_page' => $paginator->currentPage(),
            'per_page' => $paginator->perPage(),
            'next_page_url' => $paginator->nextPageUrl(),
            'previous_page_url' => $paginator->previousPageUrl(),
        ];

        if ($paginator instanceof LengthAwarePaginator) {
            $paginationMeta['total'] = $paginator->total();
            $paginationMeta['last_page'] = $paginator->lastPage();
        }

        return self::pagination($paginator->items(), $paginationMeta, $meta);
    }

    /**
     * @param array $data
     * @param array $meta
     * @param array $headers
     * @return OkResponse
     */
    public static function index(array $data, array $meta = [], array $headers = []): OkResponse
    {
        return new OkResponse('Found', 200, $data, $meta, $headers);
    }

    /**
     * @param array $data
     * @param array $meta
     * @param array $headers
     * @return OkResponse
     */
    public static function update(array $data, array $meta = [], array $headers = []): OkResponse
    {
        if (empty($data) && empty($meta)) {
            return new OkResponse('Updated', 204, [], [], $headers);
        }

        return new OkResponse('Updated', 202, $data, $meta, $headers);
    }

    /**
     * @param array $data
     * @param array $meta
     * @param array $headers
     * @return OkResponse
     */
    public static function create(array $data, array $meta = [], array $headers = []): OkResponse
    {
        if (empty($data) && empty($meta)) {
            return new OkResponse('Created', 204, [], [], $headers);
        }

        return new OkResponse('Created', 201, $data, $meta, $headers);
    }

    /**
     * @param array $headers
     * @return OkResponse
     */
    public static function delete(array $headers = []): OkResponse
    {
        return new OkResponse('Deleted', 204, [], [], $headers);
    }

    /**
     * @param array $headers
     * @return ClientErrorResponse
     */
    public static function notFound(array $headers = []): ClientErrorResponse
    {
        return new ClientErrorResponse('Not found', 404, [], [], $headers);
    }

    /**
     * @param array $headers
     * @return ClientErrorResponse
     */
    public static function notAuthorized(array $headers = []): ClientErrorResponse
    {
        return new ClientErrorResponse('Not authorized', 401, [], [], $headers);
    }

    /**
     * @param array $headers
     * @return ClientErrorResponse
     */
    public static function accessForbidden(array $headers = []): ClientErrorResponse
    {
        return new ClientErrorResponse('Access forbidden', 403, [], [], $headers);
    }
}
