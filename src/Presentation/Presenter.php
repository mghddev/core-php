<?php

namespace XCompany\Core\Presentation;

interface Presenter
{
    public function present(): array;
}
