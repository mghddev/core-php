<?php

namespace XCompany\Core\Presentation;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use XCompany\Core\Application\Filter;
use XCompany\Core\Application\FilterOperator;
use XCompany\Core\Application\Sort;

/**
 * Class QueryRequestParser
 * @package XCompany\Core\Presentation
 */
final class QueryRequestParser
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getFilters(): array
    {
        $filters = $this->request->input('filter', []);

        if (!is_array($filters) || empty($filters)) {
            return [];
        }

        $output = [];

        foreach ($filters as $name => $value) {
            $parts = explode('|', $value);
            foreach ($parts as $part) {
                $output[] = $this->parseFilterPart($name, $part);
            }
        }

        return collect($output)->unique(function (Filter $filter) {
            return $filter->getField() . ':' . $filter->getOperator()->getOperator();
        })->toArray();
    }

    private function parseFilterPart(string $field, string $part)
    {
        $parts = explode(':', $part);
        $field = Str::camel($field);

        if (count($parts) !== 2) {
            return Filter::equals($field, $parts[0]);
        }

        $operator = $this->parseFilterOperator($parts[1]);
        $value = $parts[0];

        if ($operator->isNotIn() || $operator->isIn()) {
            $value = array_map(function ($item) {
                return trim($item);
            }, explode(',', str_replace(['[', ']', '{' , '}'], '', $value)));
        }

        return new Filter($field, $operator, $value);
    }

    private function parseFilterOperator($operator): FilterOperator
    {
        switch (trim($operator)) {
            case 'eq':
                return FilterOperator::eq();
            case 'neq':
                return FilterOperator::neq();
            case 'gt':
                return FilterOperator::gt();
            case 'gte':
                return FilterOperator::gte();
            case 'lt':
                return FilterOperator::lt();
            case 'lte':
                return FilterOperator::lte();
            case 'in':
                return FilterOperator::in();
            case 'nin':
                return FilterOperator::nin();
            default:
                return FilterOperator::eq();
        }
    }

    public function getSorts(): array
    {
        if (!($sorts = $this->request->input('sort'))) {
            return [];
        }

        return array_map(function ($item) {
            $item = trim($item);
            return Str::startsWith($item, '-')
                ? Sort::desc(Str::camel(str_replace('-', '', $item)))
                : Sort::asc(Str::camel($item));
        }, explode(',', $sorts));
    }

    public function getCurrentPage(): int
    {
        if (!($page = $this->request->input('page')) && !is_numeric($page)) {
            return 1;
        }

        return abs((int)$page);
    }

    public function getPerPage(): int
    {
        if (!($perPage = $this->request->input('per_page')) && !is_numeric($perPage)) {
            return 1;
        }

        return abs((int)$perPage);
    }

    public function getFormat(): ?string
    {
        return $this->request->input('format');
    }

    public function getTerm(): ?string
    {
        return $this->request->input('q');
    }
}
