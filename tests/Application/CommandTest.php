<?php

namespace XCompany\Core\Tests\Application;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Fluent;
use Orchestra\Testbench\TestCase;
use XCompany\Core\Application\Command;
use XCompany\Core\Application\CommandHandler;
use XCompany\Core\Application\CommandValidator;
use XCompany\Core\Application\Concerns\GuessCommandName;
use XCompany\Core\Application\Concerns\PayloadAccessibleQuery;
use XCompany\Core\Application\GenericCommand;
use XCompany\Core\Common\Uuid;
use XCompany\Core\Infrastructure\Application\InMemoryCommandHandlerLocator;
use XCompany\Core\Infrastructure\Application\SimpleCommandBus;
use XCompany\Core\Infrastructure\LaravelValidator;
use XCompany\Core\Infrastructure\ValidationRules\UuidRule;

final class CommandTest extends TestCase
{
    public function testGetCommandName(): void
    {
        $command = new GenericCommand('MyCommand', []);
        $this->assertEquals($command->commandName(), 'MyCommand');

        $command = new DoSomething('kffwdggf', 'mori@gmail.com');
        $this->assertEquals($command->commandName(), 'DoSomething');
    }

    public function testCommandCarriesCorrectData(): void
    {
        $command = new DoSomething('xa23', 'morilog.ir@gmail.com');
        $this->assertEquals('xa23', $command->get('userId'));
        $this->assertEquals('morilog.ir@gmail.com', $command->get('email'));
        $this->assertEquals('morilog.ir@gmail.com', $command->get('email'));
        $this->assertCount(2, $command->all());
    }

    public function testHandleCommand(): void
    {
        $validator = new DoSomethingValidator($this->app->make(Factory::class));
        $handler = new DoSomethingHandler($validator);
        $command = new DoSomething(Uuid::generate(), 'mori@gmail.com');
        $this->assertEquals('handled', $handler->handle($command));
    }

    public function testRunWithCommandBus()
    {
        $validator = new DoSomethingValidator($this->app->make(Factory::class));
        $commandBus = new SimpleCommandBus(
            new InMemoryCommandHandlerLocator([
                'DoSomething' => new DoSomethingHandler($validator)
            ])
        );

        $result = $commandBus->dispatch(new DoSomething(Uuid::generate(), 'mori@gmail.com'));

        $this->assertEquals('handled', $result);
    }
}


final class DoSomething implements Command
{
    use GuessCommandName;
    use PayloadAccessibleQuery;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $email;

    public function __construct(string $userId, string $email)
    {
        $this->userId = $userId;
        $this->email = $email;
    }
}

final class DoSomethingValidator extends LaravelValidator
{
    public function rules(Fluent $input): array
    {
        return [
            'userId' => ['required', new UuidRule()],
            'email' => ['required', 'email'],
        ];
    }
}



final class DoSomethingHandler implements CommandHandler
{
    /**
     * @var DoSomethingValidator
     */
    private $validator;

    public function __construct(DoSomethingValidator $validator)
    {
        $this->validator = $validator;
    }

    public function handle(Command $command)
    {
        return 'handled';
    }
}
