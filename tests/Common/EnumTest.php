<?php

namespace XCompany\Core\Tests\Common;

use PHPUnit\Framework\TestCase;
use XCompany\Core\Common\Enum;

final class EnumTest extends TestCase
{
    public function testCreateEnumWithKey(): void
    {
        $first = StringEnum::first();
        $this->assertTrue($first instanceof StringEnum);

        $this->expectException(\BadMethodCallException::class);
        $third = StringEnum::third();
    }

    public function testGetKey(): void
    {
        $first = StringEnum::first();
        $this->assertEquals($first->key(), 'FIRST');
    }

    public function testGetValue(): void
    {
        $second = StringEnum::second();
        $this->assertEquals($second->value(), 'second');
    }

    public function testCheckerMethod(): void
    {
        $first = StringEnum::first();
        $this->assertTrue($first->isFirst());
        $second = StringEnum::second();
        $this->assertTrue($second->isSecond());
    }

    public function testEqualsTo(): void
    {
        $first = StringEnum::first();
        $other = new StringEnum('first');
        $this->assertTrue($other->equalsTo($first));
    }
}

final class StringEnum extends Enum
{
    const FIRST = 'first';
    const SECOND = 'second';
}