<?php

namespace XCompany\Core\Tests\Common;

use PHPUnit\Framework\TestCase;
use XCompany\Core\Common\NationalCode;

final class NationalCodeTest extends TestCase
{
    public function testCreateCode(): void
    {
        $code = new NationalCode('2610036175');
        $this->assertEquals($code->value(), '2610036175');
    }

    public function testValidate(): void
    {
        $this->assertTrue(NationalCode::validate('2610036175'));
        $this->assertFalse(NationalCode::validate('26100361'));
        $this->assertFalse(NationalCode::validate('093967225'));
    }
}
