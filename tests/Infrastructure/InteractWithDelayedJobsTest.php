<?php

namespace XCompany\Core\Tests\Infrastructure;

use Illuminate\Database\Eloquent\Model;
use Orchestra\Testbench\TestCase;
use XCompany\Core\Infrastructure\Domain\InteractWithDelayedJobs;

final class InteractWithDelayedJobsTest extends TestCase
{
    public function testRunJobDelayed()
    {
        $parent = new Father(['name' => 'john']);
        $parent->addChild('alex');

        $this->assertNotEmpty($parent->getDelayedJobs());
        $this->assertEmpty($parent->getDelayedJobs());
    }
}

final class Father extends Model
{
    use InteractWithDelayedJobs;

    protected $guarded = [];

    public function addChild($name)
    {
        $this->runDelayed(function (Father $father) use ($name) {
            $father->children()->save(new Child([
                'name' => $name,
            ]));
        });
    }

    public function children()
    {
        return $this->hasMany(Child::class);
    }
}

final class Child extends Model
{
    protected $guarded = [];
}
