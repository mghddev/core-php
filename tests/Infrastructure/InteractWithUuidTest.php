<?php

namespace XCompany\Core\Tests\Infrastructure;

use Illuminate\Database\Eloquent\Model;
use Orchestra\Testbench\TestCase;
use XCompany\Core\Common\Uuid;
use XCompany\Core\Infrastructure\Domain\InteractWithUuid;

final class InteractWithUuidTest extends TestCase
{
    public function testInteractWithUuid()
    {
        $model = new DumbModel([
            'id' => Uuid::generate(),
            'user_id' => Uuid::generate()
        ]);

        $this->assertInstanceOf(Uuid::class, $model->getUuid('user_id'));
        $this->assertInstanceOf(Uuid::class, $model->getUuid('id'));
        $this->assertFalse(ctype_print($model->id));
        $this->assertFalse(ctype_print($model->user_id));
    }
}

final class DumbModel extends Model
{
    use InteractWithUuid;
    protected $uuids = ['id', 'user_id'];
    protected $guarded = [];
    public $incrementing = false;
}
