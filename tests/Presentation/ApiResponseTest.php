<?php

namespace XCompany\Core\Tests\Presentation;

use Faker\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\Paginator;
use Orchestra\Testbench\TestCase;
use XCompany\Core\Presentation\ApiResponse\ResponseFactory;

final class ApiResponseTest extends TestCase
{
    public function testTestCreateResponse(): void
    {
        $response = ResponseFactory::create(['userId' => 1]);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals(['userId' => 1], $response->getData());

        $response = ResponseFactory::create([]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testNotFoundResponse(): void
    {
        $response = ResponseFactory::notFound();
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testIndexResponse(): void
    {
        $response = ResponseFactory::index(['userId' => 1]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testFromLaravelPaginatorResponse(): void
    {
        $faker = Factory::create();
        $items = [];
        for ($i = 0; $i < 20; $i++) {
            $items[] = [
                'email' => $faker->email,
                'first_name' => $faker->firstName
            ];
        }

        $pagination = new Paginator($items, 10, 1);

        $response = ResponseFactory::fromLaravelPaginator($pagination);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('pagination', $response->getMeta());
        $this->assertEquals(1, $response->getMeta()['pagination']['current_page']);
    }

    public function testNotAuthorized(): void
    {
        $response = ResponseFactory::notAuthorized();
        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testToJsonResponse(): void
    {
        $response = ResponseFactory::notFound();
        $this->assertInstanceOf(JsonResponse::class, $response->toJsonResponse());
    }

    public function testWithHeader(): void
    {
        $response = ResponseFactory::notFound()
            ->withHeader('X-User-Id', 1);

        $this->assertEquals($response->getHeaders(), ['X-User-Id' => 1]);
    }
}
