<?php

namespace XCompany\Core\Tests\Presentation;

use Illuminate\Http\Request;
use Orchestra\Testbench\TestCase;
use XCompany\Core\Application\Filter;
use XCompany\Core\Presentation\QueryRequestParser;

final class QueryRequestParserTest extends TestCase
{
    public function testGetFilters()
    {
        $request = Request::createFromGlobals();
        $request->merge([
            'filter' => [
                'age' => '10:gte|25:lte',
                'skills' => '{programming, cooking}:in|{football, basketball}:nin',
                'name' => 'morilog'
            ]
        ]);

        $parser = new QueryRequestParser($request);
        $filters = $parser->getFilters();

        $this->assertCount(5, $filters);
        foreach ($filters as $filter) {
            $this->assertTrue($filter instanceof Filter);
        }

        $this->assertTrue($filters[2]->getOperator()->isIn());
        $this->assertTrue(is_array($filters[2]->getValue()));
    }

    public function testGetSorts()
    {
        $request = Request::createFromGlobals();
        $request->merge([
            'filter' => [
                'age' => '10:gte|25:lte',
                'skills' => '{programming, cooking}:in|{football, basketball}:nin',
                'name' => 'morilog'
            ],
            'sort' => 'created,order_id, -request_at'
        ]);

        $parser = new QueryRequestParser($request);
        $sorts = $parser->getSorts();

        $this->assertCount(3, $sorts);
        $this->assertFalse($sorts[0]->isDesc());
        $this->assertFalse($sorts[1]->isDesc());
        $this->assertTrue($sorts[2]->isDesc());
    }
}
